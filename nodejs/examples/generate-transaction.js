const axios = require('axios');
const jwt = require('../helpers/jwt');

const paymentRequestBody = `
{
  "role": "DST",
  "account": "payeeAccount",
  "friendlyName": "payeeFriendlyName",
  "callbackUrl": "https://callback.eu",
  "trData": {
     "currency": "USD",
     "reasonL1": "textReasonL1",
     "reasonL2": "textReasonL2"
  },
  "trOptions": {
     "type": "QR"
  }
}
`.replace(/\s/g, '');
const token = jwt.createJwt(paymentRequestBody);
const headers = {
  'Content-Type': 'application/json',
  'Authorization': `Bearer ${token}`,
  'Api-Version': '1'
};

axios.post('https://sandbox.payware.eu/api/transactions', paymentRequestBody, { headers })
  .then(response => console.log('SUCCESS!', response))
  .catch(error => console.log('ERROR!', error));
