const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const config = require('./config');

function createJwt(requestBody) {
  const privateKey = '-----BEGIN PRIVATE KEY-----\n' + config.privateKey + '\n-----END PRIVATE KEY-----';

  const contentMd5 = createMd5Hash(requestBody);
  const payload = {
    iss: config.partnerId,
    aud: 'https://payware.eu',
    iat: new Date().getTime()
  };
  return jwt.sign(payload, privateKey, { algorithm: 'RS256', header: { contentMd5 } });
}

function createMd5Hash(requestBody) {
  const md5hash = crypto.createHash('md5').update(requestBody.replace(/\s/g, '')).digest('binary');//;
  return Buffer.from(md5hash, 'binary').toString('base64');
}

module.exports = { createJwt };
