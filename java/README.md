# payware API Java Integration Example

## Build

`./mvnw clean install`

***Note:*** Java 11+ required.

## Run

`java -cp target/payware-java-demo-0.0.1-SNAPSHOT-jar-with-dependencies.jar eu.payware.examples.GenerateTransaction`
