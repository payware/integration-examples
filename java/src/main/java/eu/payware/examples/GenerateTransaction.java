package eu.payware.examples;

import eu.payware.helpers.JwtHelper;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Scanner;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class GenerateTransaction {

  public static void main(String... args) throws IOException, InterruptedException, NoSuchAlgorithmException, InvalidKeySpecException {
    HttpClient client = HttpClient.newHttpClient();

    String json = getFromJson("/generate-transaction.json");
    String jwt = JwtHelper.createJwt(json);

    HttpRequest request = HttpRequest.newBuilder(URI.create("https://sandbox.payware.eu/api/transactions"))
        .POST(HttpRequest.BodyPublishers.ofString(json))
        .header("Content-Type", "application/json")
        .header("Authorization", "Bearer " + jwt)
        .header("Api-Version", "1").build();

    HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

    System.out.println(response.body());
  }

  public static String trim(String source) {
    return source.replaceAll("\\s", "");
  }

  public static String inputStreamToString(InputStream is) {
    try (Scanner s = new Scanner(is).useDelimiter("\\A")) {
      return s.hasNext() ? s.next() : "";
    }
  }

  public static String getFromJson(String fileName) {
    try {
      return trim(inputStreamToString(GenerateTransaction.class.getResourceAsStream(fileName)));
    } catch (Exception ex) {
      throw new RuntimeException("Could not load json from file: " + fileName);
    }
  }

}
