package eu.payware.helpers;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public final class Base64Util {

  public static String encodeToString(String s) {
    var bytes = s.getBytes(StandardCharsets.UTF_8);
    return encodeToString(bytes);
  }

  public static String encodeToString(byte[] bytes) {
    return new String(
        Base64.getEncoder().encode(bytes),
        StandardCharsets.UTF_8);
  }

}
