package eu.payware.helpers;

import java.util.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class JwtHelper {

  private static PrivateKey getPrivateKey(String base64encodedKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
    byte[] byteKey = Base64.getDecoder().decode(base64encodedKey);
    var key = new PKCS8EncodedKeySpec(byteKey);
    var keyFactory = KeyFactory.getInstance("RSA");
    return keyFactory.generatePrivate(key);
  }

  private static String md5hash(final String message) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("MD5");
    md.update(message.getBytes(StandardCharsets.UTF_8));
    return Base64Util.encodeToString(md.digest());
  }

  public static String createJwt(String requestBody) throws NoSuchAlgorithmException, InvalidKeySpecException {
    Map<String, Object> headers = new HashMap<>();
    headers.put("typ", "JWT");
    headers.put("contentMd5", md5hash(requestBody));

    PrivateKey privateKey = getPrivateKey(Config.PRIVATE_KEY);
    String audience = "https://payware.eu";

    return Jwts.builder()
      .setIssuer(Config.PARTNER_ID)
      .setAudience(audience)
      .setHeader(headers)
      .signWith(SignatureAlgorithm.RS256, privateKey)
      .setIssuedAt(new Date())
      .compact();
  }

}
